;;;; SPDX-FileCopyrightText: 2016 Vasilij Schneidermann <mail@vasilij.de>
;;;;
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(define (apropos-completions prefix)
  (let ((candidates (apropos-list `(: bos ,prefix) #:macros? #t)))
    (remove
     (lambda (candidate) (substring-index "#" candidate))
     (map symbol->string candidates))))

(define (current-environment-completions prefix)
  (let ((size (string-length prefix)))
    (filter
     (lambda (candidate)
       (and (<= size (string-length candidate))
            (substring=? prefix candidate 0 0)))
     (map (o symbol->string car) (##sys#current-environment)))))

(define (make-completer #!rest procs)
  (let ((completions #())
        (index 0))
    (lambda (prefix state)
      (when (zero? state)
        (let ((candidates (append-map (lambda (proc) (proc prefix)) procs)))
          (set! completions (list->vector candidates)))
        (set! index 0))
      (if (< index (vector-length completions))
          (let ((completion (vector-ref completions index)))
            (set! index (add1 index))
            completion)
          #f))))

(define scheme-completer
  (make-completer apropos-completions current-environment-completions))
