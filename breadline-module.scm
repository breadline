;;;; SPDX-FileCopyrightText: 2016 Vasilij Schneidermann <mail@vasilij.de>
;;;;
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(module breadline
  (history-length history-file add-history! read-history! write-history!
   stifle-history! unstifle-history!
   completer-set! completer-word-break-characters-set!
   line-buffer point end
   display-prompt
   variable-bind! variable-value
   event-hook-set! pre-input-hook-set!
   redisplay-function-set! redisplay
   insert-text delete-text stuff-char
   basic-quote-characters-set! paren-blink-timeout-set!
   cleanup-after-signal! reset-after-signal! reset-terminal!
   readline make-readline-port)

  (import scheme)
  (import (chicken base))
  (import (chicken condition))
  (import (chicken file))
  (import (chicken foreign))
  (import (chicken format))
  (import (chicken gc))
  (import (chicken port))
  (import (chicken repl))
  (import (srfi 18))

  (include "breadline.scm"))
