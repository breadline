;;;; SPDX-FileCopyrightText: 2016 Vasilij Schneidermann <mail@vasilij.de>
;;;;
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(module breadline-scheme-completion
  (scheme-completer)

  (import scheme)
  (import (chicken base))
  (import (chicken string))
  (import (srfi 1))
  (import apropos)
  (import breadline)

  (include "breadline-scheme-completion.scm"))
