<!--
SPDX-FileCopyrightText: 2016 Vasilij Schneidermann <mail@vasilij.de>

SPDX-License-Identifier: GPL-3.0-or-later
-->

![Bowery men waiting for bread in bread line, New York City, Bain Collection][picture]

## About

A better GNU Readline egg.

## Docs

See [its wiki page].

[picture]: img/breadline.jpg
[its wiki page]: http://wiki.call-cc.org/eggref/5/breadline
