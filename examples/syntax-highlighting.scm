;;;; SPDX-FileCopyrightText: 2016 Vasilij Schneidermann <mail@vasilij.de>
;;;;
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(import scheme)
(import (chicken base))
(import (chicken format))
(import (chicken io))
(import (chicken pathname))
(import (chicken process))
(import (chicken process-context))
(import (chicken string))
(import (prefix breadline rl:))
(import medea)

(define python-file (pathname-replace-extension (program-name) ".py"))

(define-values (server-in server-out server-pid)
  (process "python" (list python-file)))

(define (close-server!)
  (when (output-port-open? server-out)
    (write-line "quit" server-out)
    (close-output-port server-out))
  (when (input-port-open? server-in)
    (close-input-port server-in)))

(define (highlight text)
  (write-json (vector text) server-out)
  (newline server-out)
  (flush-output server-out)
  (string-chomp (vector-ref (read-json (read-line server-in)) 0)))

(define (redisplay-with-highlight!)
  (rl:redisplay)
  (printf "~c[2K~c~a~a~c[~aD"
          #\escape
          #\return
          (rl:display-prompt)
          (highlight (rl:line-buffer))
          #\escape
          (- (rl:end) (rl:point)))
  (when (= (rl:end) (rl:point))
    (printf "~c[1C" #\escape))
  (flush-output))

(rl:redisplay-function-set! redisplay-with-highlight!)

(let loop ()
  (let ((input (rl:readline "> ")))
    (when input
      (print input)
      (loop))))

(newline)
(close-server!)
