# SPDX-FileCopyrightText: 2016 Vasilij Schneidermann <mail@vasilij.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import json
import signal
import sys

import pygments
from pygments.formatters import TerminalFormatter
from pygments.lexers.lisp import SchemeLexer


def signal_handler(_sig, _frame):
    sys.exit(0)


def main():
    lexer = SchemeLexer()
    formatter = TerminalFormatter()
    while True:
        line = sys.stdin.readline().rstrip()
        if line == 'quit':
            break
        code = json.loads(line)[0]
        highlighted = pygments.highlight(code, lexer, formatter)
        sys.stdout.write(json.dumps([highlighted]))
        sys.stdout.write('\n')
        sys.stdout.flush()


if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    main()
