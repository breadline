REM SPDX-FileCopyrightText: 2016 Vasilij Schneidermann <mail@vasilij.de>
REM
REM SPDX-License-Identifier: GPL-3.0-or-later

"%CHICKEN_CSC%" -C "%READLINE_CFLAGS%" -L "%READLINE_LDLIBS%" %*
